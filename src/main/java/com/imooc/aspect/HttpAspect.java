package com.imooc.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by nana on 2017/10/27.
 */
@Aspect
@Component
public class HttpAspect {
    /*@Before("execution(public * com.imooc.controller.GirlController.*(..))")
    public void log(){
        System.out.println("1111");
    }
    @After("execution(public * com.imooc.controller.GirlController.*(..))")
    public void doAfter(){
        System.out.print("2222");
    }*/

    Logger log = LoggerFactory.getLogger(HttpAspect.class);
    @Pointcut("execution(public * com.imooc.controller.GirlController.girlAdd())")
    public void log(){

    }

    @Before("log()")
    public void doBefore(JoinPoint joinPoint) {
       // log.info("1111");
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        //url
        log.info("url={}",request.getRequestURL());

        //method
        log.info("method={}",request.getMethod());

        //ip
        log.info("ip={}",request.getRemoteAddr());

        //类方法
        log.info("class_method={}",joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName() );

        //参数
        log.info("args={}",joinPoint.getArgs());
    }

    @After("log()")
    public void doAfter(){
        log.info("2222");
    }

    @AfterReturning(returning = "object", pointcut="log()")
    public void doAfterReturning(Object object){
        log.info("response={}",object.toString());
    }
}
