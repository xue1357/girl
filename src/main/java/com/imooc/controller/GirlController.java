package com.imooc.controller;

import com.imooc.domain.Result;
import com.imooc.repository.GirlRepository;
import com.imooc.service.GirlService;
import com.imooc.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.imooc.domain.Girl;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by nana on 2017/10/26.
 */
@RestController
public class GirlController {

    @Autowired
    private GirlRepository girlRepository;

    @Autowired
    private GirlService girlService;

    @GetMapping(value = "/girls")
    public List<Girl> girlList(){
        System.out.println("查询所有数据");
        return girlRepository.findAll();
    }

    @PostMapping(value = "/girlAdd")
    public Girl girlAdd(@RequestParam(value="cupSize") String cupSize, @RequestParam(value="age") Integer age){
        System.out.println("插入数据");
        Girl girl = new Girl();
        girl.setCupSize(cupSize);
        girl.setAge(age);

        return girlRepository.save(girl);
    }
   /* @PostMapping(value = "/girls")
    public Result<Girl> girlAdd(@Valid Girl girl, BindingResult bindingResult){
        System.out.println(girl.toString());
        if(bindingResult.hasErrors()) {
            return ResultUtil.error(1,bindingResult.getFieldError().getDefaultMessage());
        }
        girl.setCupSize(girl.getCupSize());
        girl.setAge(girl.getAge());
        return ResultUtil.success(girlRepository.save(girl));
    }*/


    @GetMapping(value = "/girls/{id}")
    public Girl queryOne(@PathVariable("id") Integer id)
    {
        System.out.println("根据id查询数据");
        return girlRepository.findOne(id);
    }

    @GetMapping(value = "/girls/age/{age}")
    public List<Girl> findListAge(@PathVariable("age") Integer age)
    {
        System.out.println("根据age查询数据");
        return girlRepository.findByAge(age);
    }


    @PutMapping(value = "/girls/{id}")
    public Girl updateOne(@PathVariable("id") Integer id,
                          @RequestParam("cupSize") String cupSize, @RequestParam("age") Integer age){
        System.out.println("根据id更新数据");
        Girl girl = new Girl();
        girl.setId(id);
        girl.setCupSize(cupSize);
        girl.setAge(age);

        return girlRepository.save(girl);
    }

    @DeleteMapping(value = "/girl/{id}")
    public void deleteOne(@PathVariable("id") Integer id) {
        System.out.println("根据id删除数据");
        girlRepository.delete(id);
    }

    @PostMapping(value = "/girls/two")
    public void girlTwo(){
        girlService.insertTwo();
    }
}
