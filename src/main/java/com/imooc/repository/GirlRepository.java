package com.imooc.repository;

import com.imooc.domain.Girl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by nana on 2017/10/26.
 */
public interface GirlRepository extends JpaRepository<Girl,Integer>{
    //通过年龄查找
    public List<Girl> findByAge(Integer age);
}
